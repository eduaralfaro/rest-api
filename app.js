const express = require('express');
const mysql = require('mysql');

const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ventas'
});




connection.connect(error => {
    if (error) throw error;
    console.log('Database Server Running')
});

app.listen(PORT, () => console.log(`Server  running on Port ${PORT}`));

app.get('/', (req, res) => {
    res.send('Welcome To My Api');

});


app.get('/sales', (req, res) => {
   
    const sql = 'SELECT * From sales';
    connection.query(sql, (error, results)=>{
if(error)throw error;
if(results.length > 0){
    res.json(results);
}else{
    res.send('not result');
}
    });
});
app.get('/sales/:id', (req, res) => {
    const { id } = req.params;
    const sql = `SELECT * FROM sales WHERE id = ${id}`;
    connection.query(sql, (error, result) => {
      if (error) throw error;
  
      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('Not result');
      }
    });
});
app.post('/add', (req, res) => {
    const sql = 'INSERT INTO sales SET ?';

  const salesObj = {
    precio: req.body.precio,
    producto: req.body.producto
  };

  connection.query(sql, salesObj, error => {
    if (error) throw error;
    res.send('sales created!');
  });
});

app.put('/update/:id', (req, res) => {
    const { id } = req.params;
    const { precio, producto } = req.body;
    const sql = `UPDATE sales SET precio = '${precio}', producto='${producto}' WHERE id =${id}`;

  connection.query(sql, error => {
    if (error) throw error;
    res.send('sales updated!');
  });
});

app.delete('/delete/:id', (req, res) => {
    const { id } = req.params;
    const sql = `DELETE FROM sales WHERE id= ${id}`;

  connection.query(sql, error => {
    if (error) throw error;
    res.send('Delete sale');
  });
});



